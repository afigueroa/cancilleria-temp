$( document ).ready(function() {
  $('#overlay').hide();
  $('input[id=validacion]').attr('placeholder','R.I.F , C.I. ó Pasaporte.');
  $('#aviso').modal('show');
  $('#second').hide();

  $('#instructivo').on('click',function(){
    $('#first').hide();
    $('#inicio').removeClass('active');
    $('#second').show();
    $('#instructivo').addClass('active');

  });

  $('#inicio').on('click',function(){
    $('#first').show();
    $('#inicio').addClass('active');
    $('#second').hide();
    $('#instructivo').removeClass('active');
  });

  validator = {
    1: [0,1],
    2: [2,3],
    3: [4,5],
    4: [6,7],
    5: [8,9],
    6: [0,1,2,3,4],
    7: [5,6,7,8,9]
  }
  var f=new Date();

  var getUrlParameter = function(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  };

  $('#validacion_boton').on('click',function(){
      var val_iden = $('#validacion').val();
      if(val_iden == '' || val_iden.length < 6){
         alert('Debe ingresar una identificacion válida');
         return false;
      }

      var ult_digi = val_iden[val_iden.length -1];
      if( getUrlParameter('idkey') == '' || getUrlParameter('idkey') == ''){
         return false;
      }else{
        var prodId = getUrlParameter('idkey');
        var prodStr = getUrlParameter('key');
        if(validator[f.getDay()].includes(parseInt(ult_digi))){
            $('#overlay').show();
            $('.form').find('input, textarea, button, select').attr('disabled','disabled');
            window.location.href='http://legalizacionve.mppre.gob.ve/'+prodId+'/'+prodStr;
        }else{
          $('#validacion_error').modal('show');
        }
      }

  });

})
